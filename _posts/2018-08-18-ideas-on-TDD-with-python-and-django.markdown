---
layout: post
title:  "Ideas on TDD with python and django"
date:   2018-08-18 13:42:01 -0500
categories: [dev, python, django, tdd]
---

This is a post to outline the TDD methodology I am currently working on for my
python and django projects. I draw heavy inspiration from Two Scoops of Django
and the TDD Goat book. I also try to follow the twelve factor app methodology
as closely as possible.

## Tools, libraries, frameworks
### On pipenv and pyenv
#### Pipenv
I have had some success with pipenv so far, although as it is not a
universally accepted and used tool in the python and django communities I have
had some reservations and have thought of switching back to pip and requirements
files. Pipenv also has a nifty feature to automatically include environment
variables from a .env file whenever it is run. This can be a nice way to implement
environment-specific state like db passwords and follow the twelve factor app
principles.
#### Pyenv
Another interesting tool that does not have community consensus. I have
used pyenv a few times and found it useful to manage my local copies of python.
At the same time, it have proven finicky when trying to install python sometimes,
mostly because of missing packages. Unfortunately, python isn't always clear on
the packages needed to compile/install it, even on their website.

### Alternatives

#### Pip + requirements files
You can use pip to install packages and requirements
files to track required packages and their versions. You can segregate production
and dev environments by using different requirements files. The best way seems to
be to put all important packages in a base.txt file, and including this file in
your production.txt, test.txt and dev.txt files, and then have these files only
specify packages unique to their environment.

#### Virtualenv + virtualenv wrapper
Virtualenv is a must, and pipenv builds on top
of it rather than replacing it. Plain virtualenv can be a pain to manage, so
virtualenvwrapper comes to the rescue to help you track and activate your envs
in a standard manner.

#### Manually compiling python versions
I think a project should never rely on the
system's intall python version. This violates twelve factor app principles. You
should instead always have a specific python version assigned to your project (
through pipenv's Pipfile or some other method), and you should ensure that version
is installed as a dependency for your application. Pyenv can help you with that,
but like I stated it can be finicky sometimes. Pyenv's ability to easily switch
active python versions also doesn't sound that useful for production, and using
it means you have another package to install and keep track of for your project.
Another solution is to manually compile python yourself during deployment. This
isn't too hard to do anyways and mostly requires installing some required packages,
fetching a tarball with wget and maybe specifying some compilation flags. Espescially
important is to make sure you don't overwrite the system's default python version.
This is done by running make altinstall instead of make install. Here's an example
install python 3.6.6

{% highlight shell %}

wget https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tgz
tar xf Python-3.6.6.tgz
cd Python-3.6.6
./configure
make
sudo make altinstall

{% endhighlight %}

This should install python 3.6.6 alongside other versions that may be installed
without overriding them.
Alternatively, you can run ./configue --prefix=/some/other/directory if you don't
want to install python in the default localtion (usually /usr/local). This is
useful if you don't have root access or want to isolate this installation to your
home folder.

{% highlight shell %}

./configure --prefix=$HOME/python3.6.6
make
make install

{% endhighlight %}

Don't forget to enable optimizations if you end up compiling python yourself in
production:

{% highlight shell %}

./configure --enable-optimizations
make
make install

{% endhighlight %}

Putting it all together, my go-to set of commands would be:

{% highlight shell %}

wget https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tgz
tar xf Python-3.6.6.tgz
cd Python-3.6.6
./configure --enable-optimizations
make
sudo make altinstall

{% endhighlight %}

Just replace the URL and Python version with the version you'll be using and you're
good to go.