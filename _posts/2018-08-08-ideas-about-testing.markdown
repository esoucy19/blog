---
layout: post
title:  "Ideas about testing"
date:   2018-08-08 15:34:01 -0500
categories: [dev, tdd]
---
I have been working on a toy project to learn Django and TDD, and have been thinking
about testing in general and the different kinds of tests you can do and what their
place in a project would be.

As a reference, I have read a bunch of books and posts about testing, with various
opinions. So far some of the most fascinating have been the Python TDD Goat Book,
and a devops oriented book called "Test-Driven Architecture with Chef".

Currently, I can imagine using the following kinds of tests in the context of a
web application:
- Unit tests
- Property tests
- Integration tests
- Functional tests

## Unit tests
These are your run of the mill TDD tests using classic testing libraries like
unittest, JUnit and such. Unit tests are quick and easy and don't require a
lot of setup to get running.

Unit tests are vital for specifying and testing the basic functionality and
building blocks of your application. You will mostly use them to define and
test classes, methods and functions. Ideally, every method should have a unit
test covering every one of it's possible use cases. Do note that if you end
up writing a large amount of tests because the method feels like it can cover
a wide range of use cases this could be a sign that you should split it up
into multiple smaller function.

## Property tests
Sometimes it can be hard to think of all the uses cases that warrant testing
for a method. Knowing exactly what inputs will yield different behaviour can
require intimate knowledge of your language and framework's quirks (javascript
rounding errors anyone?**

This is where property testing comes into play. When using property tests, you
define a range of possible inputs and parameters, then the testing library runs
a bunch of times using a random distribution of inputs, trying to find all
outlying or aberrant cases.

Property tests are extremely powerful, and my description of them is pretty reductive.
They can be used alongside unit tests, or can even completely replace them.

## Integration tests
Where unit tests test the cogs of your application, integration tests test
more complete functionality. Integration tests let you make sure that the
components of your application are properly communicating and working together.
Integration tests and functional tests are sometimes used as synonyms, but I
think it is useful to separate them. Specifically, integration tests test the application from
the developer's perspective (API, rendering, file generation, compressing, email, etc).

## Functional tests
Rather than the developer's perspective, functional tests are done from the
end user's perspective. This is where you map user stories to tests. A functional
test describes a single atomic functionality of the application, a specific scenario
that makes the application useful to end users (user authentication, adding and removing
items from a shopping cart, etc).

Functional tests are also sometimes called acceptance tests, and feature prominently in the
Behavious Driven Development (BDD) methodology.
